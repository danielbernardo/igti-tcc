import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class FileUtil {

	public static List<String> readFile(String arquivoCSV) {
		
		BufferedReader br = null;
	    String line = "";
	    List<String> lines = new ArrayList<>();
	    
		try {
			br = new BufferedReader(new FileReader(arquivoCSV));
	        
			while ((line = br.readLine()) != null)
	            lines .add(line);
	        
	    } catch (Exception e) {
	    	e.printStackTrace();
	    } finally {
	    	try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	    }
	    
	    return lines;
        
	}

	public static void writeFile(String fileName, String header, List<String> lines) {
		FileWriter file = null;
		try {
			file = new FileWriter(fileName);
			PrintWriter pw = new PrintWriter(file);
			
			pw.println(header);
			
			lines.forEach((item) -> {
				pw.println(item);
			});
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				file.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	

}
