import java.util.List;
import java.util.stream.Collectors;

public class TransformationApp {

	public static void main(String[] args) {		
		List<String> lines = FileUtil.readFile("arq_casco_comp.csv");
		
		FileUtil.writeFile(
				"susep_data.csv", 
				"cod_regiao,cod_modelo,sexo,faixa_etaria,classificacao", 
				new Transformation().transform(lines));
	}
	
	public static class Transformation {

		public List<String> transform(List<String> lines) {
			return lines.stream()
				.filter(l -> filterLines(l))
				.map(l -> mapColumns(l))
				.collect(Collectors.toList());
		}

		private boolean filterLines(String line) {
			String year = split(line)[3];
			String claims = split(line)[13];
			
			return NumberUtil.isNumeric(year)
					&& NumberUtil.isNumeric(claims)
					&& Integer.valueOf(year.trim()) >= 2008
					&& Integer.valueOf(claims) > 0;
		}

		private String mapColumns(String line) {
			String[] cols = split(line);
			return String.format(
					"%s,%s,%s,%s,%s", 
					cols[1], 
					cols[2], 
					cols[4], 
					mapRangeAge(cols[5]), 
					mapClass(cols[4], cols[13]));
		}

		private String mapClass(String gender, String claims) {
			if (Integer.valueOf(claims) > 1)
				return "ALTA";

			return "M".equalsIgnoreCase(gender) ? "MEDIA" : "BAIXA";
		}

		private String mapRangeAge(String code) {
			return NumberUtil.isNumeric(code) ? ranges[Integer.valueOf(code)] : code;
		}

		private String[] split(String line) {
			return line.split(";");
		}

		private static String[] ranges = {
				"N�o informada",
				"Entre 18 e 25 anos",
				"Entre 26 e 35 anos",
				"Entre 36 e 45 anos",
				"Entre 46 e 55 anos",
				"Maior que 55 anos"
		};
		
	}

}
