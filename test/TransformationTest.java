import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;

public class TransformationTest {

	@Test
	public void testTransformation() {
		List<String> lines = FileUtil.readFile("arq_test.csv");
		List<String> result = new TransformationApp.Transformation().transform(lines);
		
		assertThat(result.size(), is(9));
		
		assertThat(result, hasItem("  ,001004-9,M,N�o informada,MEDIA"));
		assertThat(result, hasItem("  ,001004-9,F,N�o informada,BAIXA"));
		assertThat(result, hasItem("  ,001004-9,F,N�o informada,ALTA"));
		assertThat(result, hasItem("  ,001004-9,M,N�o informada,ALTA"));
		assertThat(result, hasItem("  ,001004-9,M,Entre 18 e 25 anos,MEDIA"));
		assertThat(result, hasItem("  ,001004-9,M,Entre 26 e 35 anos,MEDIA"));
		assertThat(result, hasItem("  ,001004-9,M,Entre 36 e 45 anos,MEDIA"));
		assertThat(result, hasItem("  ,001004-9,M,Entre 46 e 55 anos,MEDIA"));
		assertThat(result, hasItem("  ,001004-9,M,Maior que 55 anos,MEDIA"));
		
	}
	
}
